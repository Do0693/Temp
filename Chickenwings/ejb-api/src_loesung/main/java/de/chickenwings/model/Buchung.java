package de.chickenwings.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.chickenwings.model.types.Kategorie;

public class Buchung implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private Date datum;

	private Kategorie kategorie;

	private Flug flug;

	private List<Person> fluggaeste = new ArrayList<>();

	private String buchungsNummer;

	private String loginName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public Kategorie getKategorie() {
		return kategorie;
	}

	public void setKategorie(Kategorie kategorie) {
		this.kategorie = kategorie;
	}

	public List<Person> getFluggaeste() {
		return fluggaeste;
	}

	public void setFluggaeste(List<Person> fluggaeste) {
		this.fluggaeste = fluggaeste;
	}

	public String getBuchungsNummer() {
		return buchungsNummer;
	}

	public void setBuchungsNummer(String buchungsNummer) {
		this.buchungsNummer = buchungsNummer;
	}

    /**
     *
     * @return Login-Name der Person, die gebucht hat.
     */
	public String getLoginName() {
		return loginName;
	}

    /**
     *
     * @param loginName Login-Name der Person, die gebucht hat.
     */
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getFlugnummer() {
		return flug.getFlugnummer();
	}

	public Flug getFlug() {
		return flug;
	}

	public void setFlug(Flug flug) {
		this.flug = flug;
	}
	
}
