package de.chickenwings.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.StringJoiner;

public class Flug implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private String flugnummer;

	private LocalDateTime startzeit;

	private LocalDateTime ankunft;

	private String startFlughafen;

	private String zielFlughafen;

	private Flugzeug flugzeug;

	private double flugpreis;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFlugnummer() {
		return flugnummer;
	}

	public void setFlugnummer(String flugnummer) {
		this.flugnummer = flugnummer;
	}

	public LocalDateTime getStartzeit() {
		return startzeit;
	}

	public void setStartzeit(LocalDateTime startzeit) {
		this.startzeit = startzeit;
	}

	public LocalDateTime getAnkunft() {
		return ankunft;
	}

	public void setAnkunft(LocalDateTime ankunft) {
		this.ankunft = ankunft;
	}

	public String getStartFlughafen() {
		return startFlughafen;
	}

	public void setStartFlughafen(String startFlughafen) {
		this.startFlughafen = startFlughafen;
	}

	public String getZielFlughafen() {
		return zielFlughafen;
	}

	public void setZielFlughafen(String zielFlughafen) {
		this.zielFlughafen = zielFlughafen;
	}

	public Flugzeug getFlugzeug() {
		return flugzeug;
	}

	public void setFlugzeug(Flugzeug flugzeug) {
		this.flugzeug = flugzeug;
	}

	public double getFlugpreis() {
		return flugpreis;
	}

	public void setFlugpreis(double flugpreis) {
		this.flugpreis = flugpreis;
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", Flug.class.getSimpleName() + "[", "]")
				.add("id=" + id)
				.add("flugnummer='" + flugnummer + "'")
				.add("startzeit=" + startzeit)
				.add("ankunft=" + ankunft)
				.add("startFlughafen='" + startFlughafen + "'")
				.add("zielFlughafen='" + zielFlughafen + "'")
				.add("flugzeug=" + flugzeug)
				.add("flugpreis=" + flugpreis)
				.toString();
	}
}
