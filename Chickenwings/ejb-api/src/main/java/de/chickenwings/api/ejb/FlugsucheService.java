package de.chickenwings.api.ejb;

import de.chickenwings.model.Flug;
import javax.ejb.Remote;
import java.util.List;

@Remote
public interface FlugsucheService {

	List<Flug> sucheFluege();
}
