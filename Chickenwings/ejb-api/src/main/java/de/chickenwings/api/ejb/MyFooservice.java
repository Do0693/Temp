package de.chickenwings.api.ejb;

import javax.ejb.Remote;

@Remote
public interface MyFooservice {

    String sayFoo();

    int findSolutionForEverything();
}
