package de.chickenwings.model.types;

public enum Kategorie {

	FIRST("First Class"),

	BUSINESS("Business Class"),

	ECONOMY("Economy Class");

	private String name;

	Kategorie(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
