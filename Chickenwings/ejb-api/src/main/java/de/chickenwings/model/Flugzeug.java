package de.chickenwings.model;

import java.io.Serializable;

import de.chickenwings.model.types.Flugzeugtyp;

public class Flugzeug implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private Flugzeugtyp flugzeugTyp;

	private int leistung;

	private int sitzplaetze;

	private int besatzung;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Flugzeugtyp getFlugzeugTyp() {
		return flugzeugTyp;
	}

	public void setFlugzeugTyp(Flugzeugtyp flugzeugTyp) {
		this.flugzeugTyp = flugzeugTyp;
	}

	public int getLeistung() {
		return leistung;
	}

	public void setLeistung(int leistung) {
		this.leistung = leistung;
	}

	public int getSitzplaetze() {
		return sitzplaetze;
	}

	public void setSitzplaetze(int sitzplaetze) {
		this.sitzplaetze = sitzplaetze;
	}

	public int getBesatzung() {
		return besatzung;
	}

	public void setBesatzung(int besatzung) {
		this.besatzung = besatzung;
	}

}
