package de.chickenwings.client;

import java.util.Properties;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MessageDrivenClient {

	private final Logger log = LoggerFactory.getLogger(MessageDrivenClient.class);

	private static final String DEFAULT_CONNECTION_FACTORY = "jms/RemoteConnectionFactory";
	private static final String DEFAULT_DESTINATION = "jms/queue/test";
	private static final String DEFAULT_USERNAME = "testuser";
	private static final String DEFAULT_PASSWORD = "test";
	private static final String INITIAL_CONTEXT_FACTORY = "org.jboss.naming.remote.client.InitialContextFactory";
	private static final String PROVIDER_URL = "remote://localhost:4447";

	public static void main(String[] args) throws NamingException, JMSException {
		MessageDrivenClient client = new MessageDrivenClient();
		client.sendMessage();

	}

	public void sendMessage() throws NamingException, JMSException {

		Connection connection = null;
		Context context = null;

		try {
			// Set up the context for the JNDI lookup
			final Properties env = new Properties();
			env.put(Context.INITIAL_CONTEXT_FACTORY, INITIAL_CONTEXT_FACTORY);
			env.put(Context.PROVIDER_URL, PROVIDER_URL);
			env.put(Context.SECURITY_PRINCIPAL, DEFAULT_USERNAME);
			env.put(Context.SECURITY_CREDENTIALS, DEFAULT_PASSWORD);
			context = new InitialContext(env);

			ConnectionFactory connectionFactory = (ConnectionFactory) context
					.lookup(DEFAULT_CONNECTION_FACTORY);

			Destination destination = (Destination) context
					.lookup(DEFAULT_DESTINATION);

			// Create the JMS connection, session, producer, and consumer
			connection = connectionFactory.createConnection(DEFAULT_USERNAME,
					DEFAULT_PASSWORD);
			Session session = connection.createSession(false,
					Session.AUTO_ACKNOWLEDGE);
			MessageProducer producer = session.createProducer(destination);
			connection.start();

			// Send the specified number of messages
			TextMessage message;
			for (int i = 0; i < 10; i++) {
				message = session.createTextMessage("Hello, World!");
				producer.send(message);
			}

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		} finally {
			if (context != null) {
				context.close();
			}

			if (connection != null) {
				connection.close();
			}
		}

	}
}
