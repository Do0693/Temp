package de.chickenwings.client;

import de.chickenwings.api.ejb.MyFooservice;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;

public class Main {

    static InitialContext createInitialContext() throws NamingException {
        Properties p = new Properties();
        p.put(Context.INITIAL_CONTEXT_FACTORY, "org.apache.openejb.client.RemoteInitialContextFactory");
        p.put(Context.PROVIDER_URL, "http://localhost:8080/tomee/ejb");
        return new InitialContext(p);
    }



    public static void main(String... args) throws NamingException {
        System.out.println("Hello from the Chickenwings EjbClient.");

        final InitialContext ctx = createInitialContext();
        MyFooservice myFooservice = (MyFooservice) ctx.lookup("MyFooserviceBeanRemote");

        myFooservice.sayFoo();
        int solutionForEverything = myFooservice.findSolutionForEverything();
        System.out.println("solutionForEverything: " + solutionForEverything);
    }

}
