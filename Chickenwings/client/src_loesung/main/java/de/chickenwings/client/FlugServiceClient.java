package de.chickenwings.client;

import java.util.List;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import de.chickenwings.api.ejb.FlugsucheService;

import de.chickenwings.model.Flug;

public class FlugServiceClient {

	/**
	 * @param args
	 * @throws NamingException
	 */
	public static void main(String[] args) throws NamingException {

		// JBoss-Variante (nur zun anschauen)
		/*
			FlugsucheService accessBean = lookupRemoteStatelessBean_FLugsucheService();
			List<Flug> fluege = accessBean.sucheFluege();
		*/

		final InitialContext ctx = createInitialContext();
		FlugsucheService flugSucheService = (FlugsucheService) ctx.lookup("FlugsucheServiceBeanRemote");
		List<Flug> fluege = flugSucheService.sucheFluege();

		System.out.println("XXX Fluege gefunden : " + fluege.size());
		for (Flug flug : fluege) {
			System.out.println("XXX Flug: " + flug);
		}
	}


	static InitialContext createInitialContext() throws NamingException {
		Properties p = new Properties();
		p.put(Context.INITIAL_CONTEXT_FACTORY, "org.apache.openejb.client.RemoteInitialContextFactory");
		p.put(Context.PROVIDER_URL, "http://localhost:8080/tomee/ejb");
		return new InitialContext(p);
	}

	/**
	 * Demo-Code zum Vergleich:<br>
	 * Hier wird eine RemoteBean im JBoss Wildfly via JNDI ermittelt.
	 *
     * Looks up and returns the proxy to remote stateless bean
     *
     * @return
     * @throws NamingException
     */
	/*
    private static FlugsucheService lookupRemoteStatelessBean_FLugsucheService() throws NamingException {
    	
    	final Properties jndiProps = new Properties();   	
		jndiProps.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
		jndiProps.put("jboss.naming.client.ejb.context", true);
		jndiProps.put(Context.PROVIDER_URL, "remote://localhost:4447");
		jndiProps.put(Context.SECURITY_PRINCIPAL, "tester");
		jndiProps.put(Context.SECURITY_CREDENTIALS, "test");
        jndiProps.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
        final Context context = new InitialContext(jndiProps);
        
        // The app name is the application name of the deployed EJBs. This is typically the ear name
        // without the .ear suffix. However, the application name could be overridden in the application.xml of the
        // EJB deployment on the server.
        final String appName = "air-adessi";
        
        // This is the module name of the deployed EJBs on the server. This is typically the jar name of the
        // EJB deployment, without the .jar suffix, but can be overridden via the ejb-jar.xml
        // In this example, we have deployed the EJBs in a air-adessi.jar, so the module name is business.jar
        final String moduleName = "business";
        
        // AS7 allows each deployment to have an (optional) distinct name. We haven't specified a distinct name for
        // our EJB deployment, so this is an empty string
        final String distinctName = "";
        
        // The EJB name which by default is the simple class name of the bean implementation class
        final String beanName = FlugSucheServiceBean.class.getSimpleName();
        
        // the remote view fully qualified class name
        final String viewClassName = FlugsucheService.class.getName();
        
        // let's do the lookup
        String lookup_name = "ejb:" + appName + "/" + moduleName + "/" + distinctName + "/" + beanName + "!" + viewClassName;
        
        System.out.println("lookup-name="+lookup_name);
        
		return (FlugsucheService) context.lookup(lookup_name);
    }
    */

}
