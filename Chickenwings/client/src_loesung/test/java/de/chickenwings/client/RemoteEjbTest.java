package de.chickenwings.client;

import de.chickenwings.api.ejb.MyFooservice;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import javax.naming.InitialContext;
import javax.naming.NamingException;

@Ignore("Erst aktivieren, wenn der Server läuft.")
public class RemoteEjbTest {

    @Test
    public void testRemoteConnection() throws NamingException {
        InitialContext initialContext = Main.createInitialContext();

        MyFooservice myFooservice = (MyFooservice) initialContext.lookup("MyFooserviceBeanRemote");

        Assert.assertNotNull(myFooservice);

        System.out.println(myFooservice.sayFoo());

        int solutionForEverything = myFooservice.findSolutionForEverything();
        Assert.assertEquals(42, solutionForEverything);
    }

}
