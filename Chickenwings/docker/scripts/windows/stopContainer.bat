@ECHO YYY START stopContainer.bat
@ECHO OFF
SET container=tomee-ejb-schulung

docker inspect -f '{{.State.Running}}' %container%
IF /I "%ERRORLEVEL%" NEQ "0" (
    @ECHO Docker container not runnning. Nothing to stop.
) ELSE (
    Docker stop %container%
    @ECHO Docker container '%container%'stopped.
)
@ECHO YYY END stopContainer.bat called