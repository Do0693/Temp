@ECHO YYY START runContainer.bat
REM docker run --rm -p 8080:8080 --name tomee-ejb-schulung tomee/ejb-schulung:1.0
@ECHO OFF
cls

SET DEBUG="n"

if "%1" == "--debug" (
    SET DEBUG="y"
)

docker run --rm ^
   -e JPDA_ADDRESS=8000 ^
   -e JPDA_TRANSPORT=dt_socket ^
   -e JPDA_SUSPEND=%DEBUG% ^
   -p 8080:8080 ^
   -p 8888:8888 ^
   -p 8000:8000 ^
   --name tomee-ejb-schulung ^
   tomee/ejb-schulung:1.0 ^
   catalina.sh jpda run

@ECHO YYY END runContainer.bat