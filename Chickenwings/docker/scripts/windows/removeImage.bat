@ECHO YYY START removeImage.bat
@ECHO OFF
SET imagename=tomee/ejb-schulung
SET tag=1.0

docker image inspect %imagename%:%tag%

IF /I "%ERRORLEVEL%" NEQ "0" (
    @ECHO docker-image '%imagename%:%tag%' does not exist
) ELSE (
    docker rmi tomee/ejb-schulung:1.0
    @ECHO docker-image '%imagename%:%tag%' removed
)
@ECHO YYY END removeImage.bat