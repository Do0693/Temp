#!/bin/bash

imagename=tomee/ejb-schulung
tag=1.0

if docker image inspect ${imagename}:${tag}; then
    docker rmi tomee/ejb-schulung:1.0
    echo removeImage.sh: docker-image '${imagename}:${tag}' removed.
else
    echo removeImage.sh: docker-image '${imagename}:${tag}' does not exist.
fi
