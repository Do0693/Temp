#!/bin/bash

# docker run --rm -p 8080:8080 --name tomee-ejb-schulung tomee/ejb-schulung:1.0

DEBUG="n"

if [[ $# -gt 0 ]]; then
    if [ "$1" == "--debug" ]; then
        DEBUG="y";
        echo runContainer.sh: running in debug-mode. Container will stop an wait for remote-client.
    fi
fi

docker run --rm \
   -e JPDA_ADDRESS=8000 \
   -e JPDA_TRANSPORT=dt_socket \
   -e JPDA_SUSPEND=${DEBUG} \
   -p 8080:8080 \
   -p 8888:8888 \
   -p 8000:8000 \
   --name tomee-ejb-schulung \
   tomee/ejb-schulung:1.0 \
   catalina.sh jpda run


echo echo runContainer.sh: Container started.