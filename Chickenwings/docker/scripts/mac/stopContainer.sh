#!/bin/bash

container=tomee-ejb-schulung

if docker inspect -f '{{.State.Running}}' ${container}; then
    docker stop ${container}
    echo stopContainer.sh: Docker container '${container}' stopped.
else
    echo stopContainer.sh: Docker container not runnning. Nothing to stop.
fi
