1. Qualifier erzeugen

@Target({ TYPE, METHOD, PARAMETER, FIELD })
@Retention(RUNTIME)
@Documented
@Qualifier
public @interface IrgendeineWaehrung {

}

2. Interface deklarieren
public interface AktuellerKurs

3. Klasse zur Implementierung des Interface unter berücksichtigt des Qualifiers

@IrgendeineWaehrung
class irgendeineWaehrungKlass implements aktuellerKurs{

}

Verwenden des Qualifiers im Testfall

@Inject
@IrgendeineWaehrung
AktuellerKurs foo;