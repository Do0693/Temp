package de.chickenwings;

import de.chickenwings.view.manager.FlugManager;
import de.chickenwings.business.HeartbeatBean;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;

import javax.ejb.EJB;
import javax.inject.Inject;

@RunWith(Arquillian.class)
public class SmokeTest {

    @EJB
    private HeartbeatBean heartbeatBean;

    @Inject
    private FlugManager flugManager;


    @Deployment
    public static JavaArchive createArchiveAndDeploy() {

        JavaArchive jar = ShrinkWrap.create(JavaArchive.class, "smokeTestPackage.jar")
                .addPackages(true, HeartbeatBean.class.getPackage(), FlugManager.class.getPackage(),
                        Logger.class.getPackage())
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");

        return jar;
    }

    @Test
    public void smoketest() {
        Assert.assertNotNull(heartbeatBean);
        Assert.assertNotNull(flugManager);
        System.out.println("XXX SmokeTest erfolgreich. EJB- und CDI-Injekt funktioniert.");
    }

}
