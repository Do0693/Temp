//
//
// Arquillian Tests
//
//

@RunWith(Arquillian.class)


@Deployment
public static JavaArchive createArchiveAndDeploy() {

	JavaArchive jar = ShrinkWrap
			.create(JavaArchive.class, "meinTestPackage.jar")
			.addPackages(true, FlugsucheServiceLocal.class.getPackage(),
					Flug.class.getPackage(), DateTime.class.getPackage())
			.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");

	System.out.println(jar.toString(true));

	return jar;

}
