package de.chickenwings.business;

import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.text.SimpleDateFormat;
import java.util.Date;

@Singleton
@Startup
public class HeartbeatBean {

    @Schedule(second="*/10", minute="*", hour="*", info="Chickenwings Heartrate")
    public void foo() {
        SimpleDateFormat sd = new SimpleDateFormat("HH:mm:ss");
        System.out.println("Chicenwings Heartbeat: " + sd.format(new Date()));
    }
}
