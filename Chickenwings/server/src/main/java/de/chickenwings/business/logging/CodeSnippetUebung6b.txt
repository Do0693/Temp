import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

1) Methode mit InjectionPoinr:

    createMyLogger(InjectionPoint ip) (
    ...
    )


2) Klasse aus dem InjectionPoint ermitteln:

    ip.getMember().getDeclaringClass()

3) nicht vergessen:

    @Produces
