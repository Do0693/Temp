package de.chickenwings.view.manager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import de.chickenwings.model.Buchung;
import de.chickenwings.model.Flug;
import de.chickenwings.model.Person;
import de.chickenwings.model.types.Kategorie;

@Named("flugManager")
@SessionScoped
public class FlugManager implements Serializable {

	private static final long serialVersionUID = 1L;

	private Buchung buchung;

	private Person fluggast;

	private Flug flug;

	private String benutzerName = "User";

	private List<Flug> fluege;

	// XXX Übung 1: injiziere erstellte Bean bzw. entsprechende View auf die Bean
	// @EJB

	// XXX Übung 2: injiziere erstellte Bean bzw. entsprechende View auf die Bean
	// @EJB

	// XXX Übung 3: injiziere erstellte Bean bzw. entsprechende View auf die Bean
	// @EJB

	// XXX Übung 6a: injiziere CDI-Bean 'Flugpreisrechner'
	// @Inject

	/**
	 * Suche nach allen Flügen
	 * 
	 * XXX @AUFGABE 1 füge hier die erstelle Bean ein die eine Liste mit allen
	 * Flügen wiedergibt und dem Feld fluege zuweist
	 */
	public void suche() {
		fluege = null;// TODO Rückgabe Fluege
	}

	/**
	 * Buche Flug
	 * 
	 * XXX @AUFGABE 2a füge hier die erstellte Bean ein und übermittel das Feld
	 * buchung
	 * 
	 * XXX @AUFGABE 3 hochzählen der Buchungsnummer
	 * 
	 * @return Navigartionsstring
	 */
	public String buchungAbschließen() {

		return "suche";
	}

	/**
	 * Suche nach Buchungen
	 * 
	 * XXX @AUFGABE 2b Suche hier nach den gebuchten Flügen unter berücksichtigt
	 * des eingeloggten Users benutzerName
	 * 
	 * @return Liste mit gebuchten Flügen
	 */
	public List<Buchung> ermittelBuchungen() {
		return new ArrayList<Buchung>();
	}

	/**
	 * Buchung stornieren
	 * 
	 * XXX @AUFGABE 2c Methode zum stonieren einer Buchung
	 * 
	 * @param buchung
	 */
	public void stornieren(Buchung buchung) {

	}

	/**
	 * ermittel Anzahl von Buchungen
	 * 
	 * XXX @AUFGABE 3
	 * 
	 * 
	 * @return anzahl Buchungen
	 * 
	 */
	public int anzahlBuchungen() {
		return 0;
	}

	/**
	 * Berechnung des Flugpreises abhängig von der Personenzahl
	 * 
	 * XXX @AUFGABE 6a
	 * 
	 * @return ermittelter Flugpreis
	 */
	public double ermittelGesamtpreis() {

		//Parameter für den Flugpreisrechner:
		// 	Anzahl der Fluggäste: this.buchung.getFluggaeste().size()
		// 	Flugpreis: this.flug.getFlugpreis()

		return 0; // Hier den Flugpreisrechner verwenden.
	}

	/**
	 * Berechnet die Anzahl freier Sitzplätze
	 * 
	 * 
	 * XXX @AUFGABE 2a Verwende hier die Methode zum ermitteln aller Buchungen
	 * 
	 * @param flug
	 *            Flug
	 * @return anzahl freier Plätze
	 */
	public int calculateFreiePlaetze(Flug flug) {
		if(flug == null) {
			return 0;
		}
		int sitzplaetze = flug.getFlugzeug().getSitzplaetze();

		// for (Buchung buchung : buchungService.getBuchungen()) {
		// if (flug.getFlugnummer().equals(buchung.getFlugnummer())) {
		// sitzplaetze = sitzplaetze - buchung.getFluggaeste().size();
		// }
		// }

		return sitzplaetze;
	}

	public void entfernePerson(Person fluggast, Buchung buchung) {
		// buchungService.entfernePerson(fluggast, buchung);
	}

	public void neuerPasagier() {
		int index = 1;
		if (buchung.getFluggaeste() != null) {
			index = buchung.getFluggaeste().size() + 1;
		}
		fluggast = new Person();
		fluggast.setVorname("Max");
		fluggast.setName("Mustermann_" + index);
		fluggast.setGeburtsdatum(new Date());

	}

	/**
	 * Buchen eines Fluges
	 * 
	 * XXX @AUFGABE 3b
	 * 
	 * @param flug Flug
	 * @return Navigationsstring
	 */
	public String bucheFlug(Flug flug) {
		this.flug = flug;
		buchung = new Buchung();
		buchung.setDatum(new Date());
		buchung.setLoginName(benutzerName);
		buchung.setFlug(flug);

		return "flugBuchung";
	}

	public void fuegePasagierHinzu() {
		if (buchung.getFluggaeste() == null) {
			buchung.setFluggaeste(new ArrayList<>());

		}

		buchung.getFluggaeste().add(fluggast);
		fluggast = null;

	}

	public List<SelectItem> getKategorien() {

		List<SelectItem> items = new ArrayList<>();

		for (Kategorie kat : Kategorie.values()) {
			items.add(new SelectItem(kat, kat.getName()));
		}

		return items;
	}

	public boolean checkButtonDisabled() {
		return buchung.getFluggaeste() == null;
	}

	public String login() {
		return "suche";
	}

	public Buchung getBuchung() {
		return buchung;
	}

	public void setBuchung(Buchung buchung) {
		this.buchung = buchung;
	}

	public Person getFluggast() {
		return fluggast;
	}

	public void setFluggast(Person fluggast) {
		this.fluggast = fluggast;
	}

	public Flug getFlug() {
		return flug;
	}

	public void setFlug(Flug flug) {
		this.flug = flug;
	}

	public String getBenutzerName() {
		return benutzerName;
	}

	public void setBenutzerName(String benutzerName) {
		this.benutzerName = benutzerName;
	}

	public List<Flug> getFluege() {
		return fluege;
	}

	public void setFluege(List<Flug> fluege) {
		this.fluege = fluege;
	}

}
