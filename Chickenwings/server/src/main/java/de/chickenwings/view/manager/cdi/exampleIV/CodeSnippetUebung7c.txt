//
//
// Code Snippets CDI
//
//


@Inherited
@InterceptorBinding
@Retention(RUNTIME)
@Target({ METHOD, TYPE })
public @interface MyCustomInteceptor {

}

@Interceptor
@AroundInvoke
public Object doSomething(InvocationContext ctx) throws Exception