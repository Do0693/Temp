package de.chickenwings.view.manager.cdi.exampleIII;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.chickenwings.business.logging.LoggerCustomFactory;

@RunWith(Arquillian.class)
public class FlugzeitEventTest {

	@Inject
	CDiEvents event;

	@Deployment
	public static JavaArchive createArchiveAndDeploy() {
		JavaArchive jar = ShrinkWrap.create(JavaArchive.class, "cdiExampleIII.jar").addPackages(true, CDiEvents.class.getPackage(), LoggerCustomFactory.class.getPackage())
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
		System.out.println(jar.toString(true));
		return jar;

	}

	@Test
	public void flugzeitAenderungTest() {
		event.aendereFlugzeit();
	}
}
