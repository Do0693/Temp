package de.chickenwings.view.manager.cdi.exampleIV;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.chickenwings.business.logging.LoggerCustomFactory;

@RunWith(Arquillian.class)
public class InterceptorTest {

	@Inject
	MyCdiBean myBean;

	@Deployment
	public static JavaArchive createArchiveAndDeploy() {
		JavaArchive jar = ShrinkWrap
				.create(JavaArchive.class, "cdiExampleIV.jar")
				.addPackage(MyCdiBean.class.getPackage())
				.addPackage(LoggerCustomFactory.class.getPackage())
				.addAsManifestResource("beans.xml");
		System.out.println(jar.toString(true));
		return jar;

	}

	@Test
	public void interceptorTest() {

		myBean.greetParticipant();
	}
}
