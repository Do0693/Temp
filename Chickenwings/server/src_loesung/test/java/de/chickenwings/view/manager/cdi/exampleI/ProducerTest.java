package de.chickenwings.view.manager.cdi.exampleI;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.chickenwings.business.logging.LoggerCustomFactory;

@RunWith(Arquillian.class)
public class ProducerTest {

	@Inject
	EjbSchulung schulung;

	@Deployment
	public static JavaArchive createArchiveAndDeploy() {
		JavaArchive jar = ShrinkWrap
				.create(JavaArchive.class, "cdiExampleI.jar")
				.addPackage(LoggerCustomFactory.class.getPackage())
				.addPackage(EjbSchulung.class.getPackage())
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
		System.out.println(jar.toString(true));
		return jar;

	}

	@Test
	public void begruessungTest() {
		System.out.println("Test I");
		System.out.println("");
		System.out.println(schulung.begruesseTeilnehmer());
		System.out.println("");
		System.out.println("");
	}

	@Test
	public void producerTest() {
		System.out.println("Test II");
		System.out.println("");
		System.out.println("Anzahl der Teilnehmer "
				+ schulung.ermittelAnzahlTeilnehmer());
		System.out.println("");
		System.out.println("");
	}

}
