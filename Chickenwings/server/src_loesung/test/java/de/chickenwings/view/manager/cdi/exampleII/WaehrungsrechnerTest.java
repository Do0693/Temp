package de.chickenwings.view.manager.cdi.exampleII;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class WaehrungsrechnerTest {

	@Inject
	Waehrungsrechner rechner;

	@Deployment
	public static JavaArchive createArchiveAndDeploy() {
		JavaArchive jar = ShrinkWrap
				.create(JavaArchive.class, "cdiExampleII.jar")
				.addPackage(Waehrungsrechner.class.getPackage())
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
		System.out.println(jar.toString(true));
		return jar;

	}

	@Test
	public void euroTest() {
		System.out.println("XXX Test I");
		System.out.println("");
		System.out.println(rechner.ermittelDollarKurs());
		System.out.println("");
		System.out.println("");
	}

	@Test
	public void dollarTest() {
		System.out.println("XXX Test II");
		System.out.println("XXX");
		System.out.println(rechner.ermittelEuroKurs());
		System.out.println("");
		System.out.println("");
	}
}
