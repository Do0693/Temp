//
//
// Arquillian Tests
//
//

@RunWith(Arquillian.class)


@Deployment
public static JavaArchive createArchiveAndDeploy() {

	JavaArchive jar = ShrinkWrap
			.create(JavaArchive.class, "meinTestPackage.jar")
			.addPackages(true, MeineBuchungenLocal.class.getPackage(),
					Flug.class.getPackage(), DateTime.class.getPackage())
			.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");

	System.out.println(jar.toString(true));

	return jar;

}


//Buchung erstellen
Buchung buchung = new Buchung();
buchung.setDatum(new Date());
buchung.setKategorie(Kategorie.BUSINESS);
buchung.setBuchungsNummer("123");
buchung.setLoginName("test");

List<Person> fluggaeste = new ArrayList<Person>();
Person person = new Person();
person.setGeburtsdatum(new Date());
person.setName("Mustermann");
person.setVorname("Max");
person.setTerrorist(false);
fluggaeste.add(person);

buchung.setFluggaeste(fluggaeste);
buchung.setFlugnummer(flug.getFlugnummer());