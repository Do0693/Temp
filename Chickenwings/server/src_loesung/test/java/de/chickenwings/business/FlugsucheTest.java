package de.chickenwings.business;

import java.util.List;

import javax.ejb.EJB;

import de.chickenwings.business.flugService.FlugsucheServiceBean;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.chickenwings.model.Flug;
import de.chickenwings.api.ejb.FlugsucheService;
import de.chickenwings.business.util.LoggingInterceptor;
import org.slf4j.LoggerFactory;

@RunWith(Arquillian.class)
public class FlugsucheTest {

	@EJB
	private FlugsucheService service;

	@Deployment
	public static JavaArchive createArchiveAndDeploy() {

		JavaArchive jar = ShrinkWrap.create(JavaArchive.class, "meinTestPackage.jar")
				//.addClass(FlugsucheTest.class)
				.addPackages(true, 
						LoggingInterceptor.class.getPackage(),
						LoggerFactory.class.getPackage(),
						FlugsucheService.class.getPackage(),
						FlugsucheServiceBean.class.getPackage(),
						Flug.class.getPackage()
						)
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
//		.addAsManifestResource(new File("target/test-classes/META-INF/test-beans.xml"));
//				.addAsManifestResource("META-INF/beans.xml");
		System.out.println(jar.toString(true));

		return jar;

	}

	@Test
	public void testeFlugSuche() {
		List<Flug> fluege = service.sucheFluege();
		Assert.assertFalse(fluege.isEmpty());
		System.err.println("XXX Anzahl Flüge: " + fluege.size());
	}

}
