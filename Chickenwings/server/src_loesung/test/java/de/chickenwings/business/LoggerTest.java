package de.chickenwings.business;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;

import de.chickenwings.business.logging.LoggerCustomFactory;

@RunWith(Arquillian.class)
public class LoggerTest {

	@Inject
	private Logger simpleLogger;
	
	@Deployment
	public static JavaArchive createArchiveAndDeploy() {

		JavaArchive jar = ShrinkWrap.create(JavaArchive.class, "meinTestPackageXX.jar")
				.addPackages(true,
						LoggerCustomFactory.class.getPackage())
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");

		System.out.println(jar.toString(true));
		return jar;
	}
	
	@Test
	public void logSomething() {
		System.out.println("XXX ein log-test...");
		simpleLogger.error("XXX Eine debug-messages aus dem SimpleLogger");
	}
	
}
