package de.chickenwings.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;

import org.junit.Assert;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.chickenwings.model.Buchung;
import de.chickenwings.model.Flug;
import de.chickenwings.model.Person;
import de.chickenwings.model.types.Kategorie;
import de.chickenwings.business.buchung.MeineBuchungenBean;
import de.chickenwings.business.buchung.MeineBuchungenLocal;
import org.slf4j.LoggerFactory;

@RunWith(Arquillian.class)
public class BuchungTest {

	@EJB
	MeineBuchungenLocal bean;

	@Deployment
	public static JavaArchive createArchiveAndDeploy() {

		JavaArchive jar = ShrinkWrap.create(JavaArchive.class, "meinTestPackageXX.jar")
				.addPackages(true, MeineBuchungenBean.class.getPackage(), Flug.class.getPackage(), LoggerFactory.class.getPackage())
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");

		System.out.println(jar.toString(true));
		return jar;
	}

	@Test
	public void testAnlegenUndLaden() {
        Flug flug = helpCreateFlug();

        // buchen
		String loginName1 = "TestUser1";
		bean.bucheFlug(helpCreateBuchung(loginName1,  "Mustermann", flug));
		List<Buchung> alleBuchungen = bean.getBuchungen();
		Assert.assertEquals("Anzahl Buchungen", 1, alleBuchungen.size());
	}

	@Test
	public void testStornieren() {
		String loginName = "TestUser1";

        Flug flug = helpCreateFlug();

        Buchung buchungZumStornieren = helpCreateBuchung(loginName,  "Mustermann2", flug);
		
		bean.bucheFlug(helpCreateBuchung(loginName,  "Mustermann", flug));
		bean.bucheFlug(buchungZumStornieren);
		bean.bucheFlug(helpCreateBuchung(loginName,  "Mustermann3", flug));
		
		List<Buchung> buchungen1 = bean.getBuchungen();
		Assert.assertEquals("Anzahl vor Stornierung", 3, buchungen1.size());
		
		bean.flugStornieren(buchungZumStornieren);
		
		List<Buchung> buchungen2 = bean.getBuchungen();
		Assert.assertEquals("Anzahl nach Stornierung", 2, buchungen2.size());
		
	}
	
	@Test
	public void testEntfernenPerson() {
		Buchung buchung = helpCreateBuchung("NochnLogin", "Mustermann2", helpCreateFlug());
		Assert.assertFalse(buchung.getFluggaeste().isEmpty());
		int anzahlPersonenVorher = buchung.getFluggaeste().size();
		Person firstPerson = buchung.getFluggaeste().get(0);
		
		bean.entfernePerson(firstPerson, buchung);
		
		Assert.assertEquals(anzahlPersonenVorher-1 , buchung.getFluggaeste().size());
		
	}

	private Flug helpCreateFlug() {
        Flug flug = new Flug();
        flug.setFlugnummer("XQ117");
        flug.setStartFlughafen("CGN");
        flug.setZielFlughafen("AYT");
        return flug;
    }

	private Buchung helpCreateBuchung(String aLoginName, String aPersonName, Flug flug) {
		Buchung buchung = new Buchung();
		buchung.setDatum(new Date());
		buchung.setKategorie(Kategorie.BUSINESS);
		buchung.setBuchungsNummer("123");
		buchung.setLoginName(aLoginName);
		buchung.setFluggaeste( helpCreate2Persons(aPersonName) );
		buchung.setFlug(flug);
		return buchung;
	}
	
	private List<Person> helpCreate2Persons(String aName) {
		Person person1 = new Person();
		person1.setGeburtsdatum(new Date());
		person1.setName(aName);
		person1.setVorname("Max");
		person1.setTerrorist(false);
		
		Person person2 = new Person();
		person2.setGeburtsdatum(new Date());
		person2.setName(aName);
		person2.setVorname("Moritz");
		person2.setTerrorist(false);	

		List<Person> fluggaeste = new ArrayList<>();
		fluggaeste.add(person1);
		fluggaeste.add(person2);
		return fluggaeste;
	}

	

}
