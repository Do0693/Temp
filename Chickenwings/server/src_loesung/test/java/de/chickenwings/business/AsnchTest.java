package de.chickenwings.business;

import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.chickenwings.model.Flug;
import de.chickenwings.business.async.AsynchLocal;
import de.chickenwings.business.async.AsynchronousBean;

@RunWith(Arquillian.class)
public class AsnchTest {

	@EJB
	AsynchLocal testBean;

	@Deployment
	public static JavaArchive createArchiveAndDeploy() {

		JavaArchive jar = ShrinkWrap.create(JavaArchive.class, "meinTestPackage.jar")
				.addPackages(true, AsynchronousBean.class.getPackage(), Flug.class.getPackage(), DateTime.class.getPackage())
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
		System.out.println(jar.toString(true));
		return jar;

	}

	@Test
	public void testAsynchMethod() throws TimeoutException {

		System.out.println("XXX Test vor Future: " + new Date());
		Future<Integer> value = testBean.berechneWert();
		System.out.println("XXX Test vor value.get(): " + new Date());
		try {
			Long duration = 1000L;
			value.get(duration, TimeUnit.MICROSECONDS);
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		System.out.println("XXX Test nach value.get(): " + new Date());

	}

}
