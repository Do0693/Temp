package de.chickenwings.view.manager.cdi.exampleI;

import javax.enterprise.inject.Produces;

public class Greeting {

	@Produces
	@TeilnehmerZahl
	public int ermittelTeilnehmerAnzahl() {
		return 20;
	}

	public String greet(String name) {
		return "Hello, " + name;
	}

}
