package de.chickenwings.view.manager.cdi.exampleIII;

import java.util.Date;

import javax.enterprise.event.Event;
import javax.inject.Inject;

public class CDiEvents {

	@Inject
	Event<FlugzeitAenderung> flugzeitEvent;

	public void aendereFlugzeit() {
		flugzeitEvent.fire(new FlugzeitAenderung(new Date()));
		System.out.println("XXX Ich habe die Flugzeit geaendert " + new Date().toString());
	}
}
