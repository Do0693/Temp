package de.chickenwings.view.manager.cdi.exampleI;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;

@Named
@RequestScoped
public class EjbSchulung {

	
	@Inject
	private Logger logger;
	
	@Inject
	private Greeting greeting;

	@Inject
	@TeilnehmerZahl
	private int anzahl;

	public int ermittelAnzahlTeilnehmer() {
		return anzahl;
	}

	public String begruesseTeilnehmer() {
		logger.info("Hello you all!!");
		return greeting.greet("Hello you all!!");
	}

	public Greeting getGreeter() {
		return greeting;
	}

	public void setGreeter(Greeting greeter) {
		this.greeting = greeter;
	}

	public int getAnzahl() {
		return anzahl;
	}

	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}

}
