package de.chickenwings.view.manager.cdi.exampleIII;

import java.util.Date;

import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.slf4j.Logger;

public class CdiEventObserverII {

	@Inject
	private Logger logger;

	public void observeFlugzeitAenderung(@Observes FlugzeitAenderung aenderung) {
		logger.info("XXX CdiEventObserverII: " + new Date().toString());
	}
}
