package de.chickenwings.view.manager.cdi.exampleIV;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import org.slf4j.Logger;

@Interceptor
@MyCustomInteceptor
public class MyInterceptor {

	@Inject
	private Logger logger;

	@AroundInvoke
	public Object doSomething(InvocationContext ctx) throws Exception {
		logger.info("XXX Mein toller Interceptor!");
		return ctx.proceed();
	}
}
