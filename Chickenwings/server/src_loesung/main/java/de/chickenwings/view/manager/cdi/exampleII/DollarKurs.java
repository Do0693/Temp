package de.chickenwings.view.manager.cdi.exampleII;

@Dollar
public class DollarKurs implements AktuellerKurs {

	@Override
	public double ermittelAktuellenKurs() {
		return 0.756372438d;
	}

}
