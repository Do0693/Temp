package de.chickenwings.view.manager.cdi.exampleIII;

import javax.enterprise.event.Observes;

public class CopyOfCdiEventObserver {

	public void observeFlugzeitAenderung(@Observes FlugzeitAenderung aenderung) {
		System.out.println("XXX CopyOfCdiEventObserver: Die Flugzeit wurde geaendert " + aenderung.getFlugzeit());
	}
}
