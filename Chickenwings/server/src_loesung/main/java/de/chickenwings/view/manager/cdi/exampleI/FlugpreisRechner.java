package de.chickenwings.view.manager.cdi.exampleI;

import java.io.Serializable;

import de.chickenwings.view.manager.cdi.exampleIV.MyCustomInteceptor;
import org.slf4j.Logger;

import javax.inject.Inject;

public class FlugpreisRechner implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Logger logger;

	@MyCustomInteceptor
	public double berechnePreis(int anzahl, double wert) {
		logger.info("XXX berechnePreis called.");
		return anzahl * wert;
	}
}
