package de.chickenwings.view.manager.cdi.exampleIII;

import java.util.Date;

public class FlugzeitAenderung {

	private final Date flugzeit;

	public FlugzeitAenderung(Date flugzeit) {
		this.flugzeit = flugzeit;
	}

	public Date getFlugzeit() {
		return flugzeit;
	}

}
