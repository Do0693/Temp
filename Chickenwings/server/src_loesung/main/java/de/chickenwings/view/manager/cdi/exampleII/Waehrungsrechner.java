package de.chickenwings.view.manager.cdi.exampleII;

import javax.inject.Inject;

public class Waehrungsrechner {

	@Inject
	@Euro
	AktuellerKurs aktuellerEuroZuDollar;

	@Inject
	@Dollar
	AktuellerKurs aktuellerDollarZuEuro;

	public String ermittelDollarKurs() {
		return "Aktueller Kurs von Euro zu Dollar "
				+ aktuellerDollarZuEuro.ermittelAktuellenKurs();
	}

	public String ermittelEuroKurs() {
		return "Aktueller Kurs von Dollar zu Euro "
				+ aktuellerEuroZuDollar.ermittelAktuellenKurs();
	}
}
