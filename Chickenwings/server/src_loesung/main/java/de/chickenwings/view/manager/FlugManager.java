package de.chickenwings.view.manager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import de.chickenwings.api.ejb.FlugsucheService;
import de.chickenwings.model.Buchung;
import de.chickenwings.model.Flug;
import de.chickenwings.model.Person;
import de.chickenwings.model.types.Kategorie;
import de.chickenwings.business.buchung.MeineBuchungenLocal;
import de.chickenwings.business.singleton.FlugSingleton;
import de.chickenwings.view.manager.cdi.exampleI.FlugpreisRechner;

@Named("flugManager")
@SessionScoped
public class FlugManager implements Serializable {

	private static final long serialVersionUID = 1L;

	private Buchung buchung;

	private Person fluggast;

	private Flug flug;

	private String benutzerName = "User";

	private List<Flug> fluege;

	// XXX Uebung 1: injiziere erstellte Bean bzw. entsprechende View auf die Bean
	@EJB
	private FlugsucheService service;

	// XXX Uebung 2: injiziere erstellte Bean bzw. entsprechende View auf die Bean
	
	@EJB
	private MeineBuchungenLocal buchungService;
	//private MeineBuchungenBean buchungService;

	// XXX Uebung 3: injiziere erstellte Bean bzw. entsprechende View auf die Bean
	@EJB
	private FlugSingleton singletonCounterService;


	// XXX Übung 6a: injiziere CDI-Bean 'Flugpreisrechner'
	@Inject
	private FlugpreisRechner rechner;

	/**
	 * Suche nach allen Fluegen
	 * 
	 * XXX @AUFGABE 1 fuege hier die erstelle Bean ein die eine Liste mit allen
	 * Fluegen wiedergibt und dem Feld fluege zuweist
	 */
	public void suche() {
		fluege = service.sucheFluege();
	}

	/**
	 * Buche Flug
	 * 
	 * XXX @AUFGABE 2a fuege hier die erstellte Bean ein und uebermittel das Feld buchung
	 * 
	 * XXX @AUFGABE 3 hochzuehlen der Buchungsnummer
	 * 
	 * @return Navigationsstring
	 */
	public String buchungAbschliessen() {

		//Aufgabe 3
		singletonCounterService.increment();
		String neueBuchungsnummer = singletonCounterService.erstelleBuchungsnummer();
		buchung.setBuchungsNummer(neueBuchungsnummer);

		//Aufgabe 2
		buchungService.bucheFlug(buchung);
		return "suche";
	}

	/**
	 * Suche nach Buchungen
	 * 
	 * XXX @AUFGABE 2b Suche hier nach den gebuchten Fluegen unter beruecksichtigt
	 * des eingeloggten Users benutzerName
	 * 
	 * @return Liste mit gebuchten Fluegen
	 */
	public List<Buchung> ermittelBuchungen() {
		return buchungService.getBuchungen();
	}

	/**
	 * Buchung stornieren
	 * 
	 * XXX @AUFGABE 2c Methode zum stonieren einer Buchung
	 * 
	 * @param buchung
	 */
	public void stornieren(Buchung buchung) {
		buchungService.flugStornieren(buchung);
	}

	/**
	 * ermittel Anzahl von Buchungen
	 * 
	 * XXX @AUFGABE 3
	 * 
	 * 
	 * @return anzahl Buchungen
	 * 
	 */
	public int anzahlBuchungen() {
		return  buchungService.getBuchungen().size();
	}

	/**
	 * Berechnung des Flugpreises abhaengig von der Personenzahl
	 * 
	 * XXX @AUFGABE 6a
	 * 
	 * @return ermittelter Flugpreis
	 */
	public double ermittelGesamtpreis() {		
		if (buchung.getFluggaeste() != null) {
			return rechner.berechnePreis(
					this.buchung.getFluggaeste().size(),
					this.flug.getFlugpreis());
		}
		return 0;
	}

	/**
	 * Berechnet die Anzahl freier Sitzplaetze
	 * 
	 * 
	 * XXX @AUFGABE 2a Verwende hier die Methode zum ermitteln aller Buchungen
	 * 
	 * @param flug
	 *            Flug
	 * @return anzahl freier Plaetze
	 */
	public int calculateFreiePlaetze(Flug flug) {
		if(flug == null) {
			return 0;
		}
		int sitzplaetze = flug.getFlugzeug().getSitzplaetze();
		for (Buchung buchung : buchungService.getBuchungen()) {
			if (flug.getFlugnummer().equals(buchung.getFlugnummer())) {
				sitzplaetze = sitzplaetze - buchung.getFluggaeste().size();
			}
		}
		return sitzplaetze;
	}

	public void entfernePerson(Person fluggast, Buchung buchung) {
		buchungService.entfernePerson(fluggast, buchung);
	}

	public void neuerPasagier() {
		int index = 1;
		if (buchung.getFluggaeste() != null) {
			index = buchung.getFluggaeste().size() + 1;
		}
		fluggast = new Person();
		fluggast.setVorname("Max");
		fluggast.setName("Mustermann_" + index);
		fluggast.setGeburtsdatum(new Date());

	}

	/**
	 * Buchen eines Fluges
	 * 
	 * XXX @AUFGABE 3b
	 * 
	 * @param flug Flug
	 * @return Navigationsstring
	 */
	public String bucheFlug(Flug flug) {
		this.flug = flug;
		buchung = new Buchung();
		buchung.setDatum(new Date());
		buchung.setLoginName(benutzerName);
		buchung.setFlug(flug);

		return "flugBuchung";
	}

	public void fuegePasagierHinzu() {
		if (buchung.getFluggaeste() == null) {
			buchung.setFluggaeste(new ArrayList<>());

		}

		buchung.getFluggaeste().add(fluggast);
		fluggast = null;

	}

	public List<SelectItem> getKategorien() {

		List<SelectItem> items = new ArrayList<>();

		for (Kategorie kat : Kategorie.values()) {
			items.add(new SelectItem(kat, kat.getName()));
		}

		return items;
	}

	public boolean checkButtonDisabled() {
		return buchung.getFluggaeste() == null;
	}

	public String login() {
		return "suche";
	}

	public Buchung getBuchung() {
		return buchung;
	}

	public void setBuchung(Buchung buchung) {
		this.buchung = buchung;
	}

	public Person getFluggast() {
		return fluggast;
	}

	public void setFluggast(Person fluggast) {
		this.fluggast = fluggast;
	}

	public Flug getFlug() {
		return flug;
	}

	public void setFlug(Flug flug) {
		this.flug = flug;
	}

	public String getBenutzerName() {
		return benutzerName;
	}

	public void setBenutzerName(String benutzerName) {
		this.benutzerName = benutzerName;
	}

	public List<Flug> getFluege() {
		//Aufgabe 1
//		return fluege;
		
		//Aufgabe 2
		return singletonCounterService.getAlleFluege();		
	}

	public void setFluege(List<Flug> fluege) {
		this.fluege = fluege;
	}


	public String getVersionText() {
		//returns the major version (2.1)
		String implementationVersion = FacesContext.class.getPackage().getImplementationVersion();

		//returns the specification version (2.1)
		String specificationVersion = Package.getPackage("com.sun.faces").getSpecificationVersion();

		//returns the minor implementation version (2.1.x)
		String minorVersion = Package.getPackage("com.sun.faces").getImplementationVersion();

		return String.format("JSF Version Impl-Version: %s, Spec-Version: %s, minor Version: %s", implementationVersion, specificationVersion, minorVersion);
	}
}
