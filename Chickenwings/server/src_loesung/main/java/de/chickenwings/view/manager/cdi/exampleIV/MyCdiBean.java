package de.chickenwings.view.manager.cdi.exampleIV;

public class MyCdiBean {

	@MyCustomInteceptor
	public void greetParticipant() {
		System.out.println("XXX Hallo Teilnehmer");
	}

}
