package de.chickenwings.view.manager.cdi.exampleII;

@Euro
public class EuroKurs implements AktuellerKurs {

	@Override
	public double ermittelAktuellenKurs() {
		return 1.3221d;
	}

}
