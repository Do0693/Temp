package de.chickenwings.view.manager.cdi.exampleIII;

import java.util.Date;

import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.slf4j.Logger;

public class CdiEventObserver {

	@Inject
	private Logger logger;

	public void observeFlugzeitAenderung(@Observes FlugzeitAenderung aenderung) {
		logger.info("XXX CdiEventObserverI: " + new Date().toString());

		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		logger.info("XXX CdiEventObserverI: Ich bin jetzt auch schon fertig " + new Date().toString());
	}
}
