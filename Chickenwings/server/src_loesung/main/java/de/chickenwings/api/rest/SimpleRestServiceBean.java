package de.chickenwings.api.rest;

/**
 * Um die unten angegebenen REST-Methoden aufzurufen,
 * einfach die URL in den Browser kopieren, oder mit SoapUI aufrufen.
 * 
 * REST-URL:
 * http://localhost:8080/airAdessi/resources/flugService/fluege
 * 
 * @author marcus
 */
/*@Stateless
@Path("/flugservice")*/
public class SimpleRestServiceBean {
/*
	@Inject
	private Logger logger;

	@EJB
	private FlugsucheServiceLocal local;

	@GET
    @Path("/message")
	public String message() {
		return "Hi from cxf-rs.";
	}

	@GET
	@Path("/fluege")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Flug> getFluege() {

		List<Buchung> buchungen = createBuchungen();
		
		List<Flug> fluege = local.sucheFluege();
		fluege.get(0).setBuchungen(buchungen);

		return fluege;
	}

	@GET
	@Path("/flug")
	@Produces({ MediaType.APPLICATION_XML })
	public List<Flug> getFlug(Flug flug) {

		List<Buchung> buchungen = createBuchungen();

		List<Flug> fluege = local.sucheFluege();
		fluege.get(0).setBuchungen(buchungen);
		return fluege;
	}

	private List<Buchung> createBuchungen() {
		Buchung buchung = new Buchung();
		buchung.setBuchungsNummer("12345");
		List<Buchung> buchungen = new ArrayList<Buchung>();
		buchungen.add(buchung);
		return buchungen;
	}
	
	

	@POST
	@Path("/bucheFlug")
	@Consumes("application/json")
	public Response bucheFlug(Buchung buchung) {

		logger.info("Buchung erhalten {}", buchung);

		return Response.status(Status.OK).build();
	}
*/
}
