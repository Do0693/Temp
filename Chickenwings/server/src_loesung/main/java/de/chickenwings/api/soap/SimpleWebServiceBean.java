package de.chickenwings.api.soap;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;

import de.chickenwings.business.flugService.FlugsucheServiceLocal;
import de.chickenwings.model.Flug;

@Stateless
@WebService(name = "kleinerTest", targetNamespace = "adesso", portName = "blaport", serviceName = "meinWebservice")
public class SimpleWebServiceBean {

	@EJB
	private FlugsucheServiceLocal local;

	
	@WebMethod()
	public List<Flug> sucheFluege(Date abflug, Date ankunft) {

		return local.sucheFluege();
	}

	/**
	 * Hinweis: Wird diese Methode bspw. via SoapUI aufegruden, ist das Datumsformat in XML
	 * folgendes: 2015-01-01T08:00:00
	 * 
	 * @param abflug
	 * @param ankunft
	 * @return
	 */
	@WebMethod(operationName = "neueMethode")
	public List<Flug> sucheFluegeII(Date abflug, Date ankunft) {

		return local.sucheFluege();
	}

	public int test42() {
		return 42;
	}

	@WebMethod(exclude = true)
	public void testII() {

	}

	@WebMethod(action = "LULU")
	public void testIII() {

	}
}
