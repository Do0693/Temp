package de.chickenwings.business;


import de.chickenwings.api.ejb.MyFooservice;

import javax.ejb.Stateless;

/**
 * Bean wird von separatem Ejb-Client-Projekt benutzt.
 */
@Stateless
public class MyFooserviceBean implements MyFooservice {

    public String sayFoo() {
        return "foo";
    }

    public int findSolutionForEverything() {
        return 42;
    }
}
