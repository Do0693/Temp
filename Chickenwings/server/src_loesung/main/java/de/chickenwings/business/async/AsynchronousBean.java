package de.chickenwings.business.async;

import java.util.concurrent.Future;

import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;

//DEMO fuer Deklaration eines EJB-Interfaces.
//@Local(de.chickenwings.business.async.Irgendwas.class)
@Stateless
public class AsynchronousBean implements AsynchLocal {

	@Asynchronous
	public Future<Integer> berechneWert() {
		System.out.println("XXX Async-Bean berechnet Wert ...");
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		final Integer value = 19;
		System.out.println("XXX Async-Bean hat Wert berechnet: " + value);
		return new AsyncResult<>(value);
	}

}
