package de.chickenwings.business.buchung;

import java.util.List;

import javax.ejb.Local;

import de.chickenwings.model.Buchung;
import de.chickenwings.model.Person;

@Local
public interface MeineBuchungenLocal {

	void bucheFlug(Buchung buchung);
	
	void flugStornieren(Buchung buchung);
	
	List<Buchung> getBuchungen();
	
	void entfernePerson(Person fluggast, Buchung buchung);
}
