package de.chickenwings.business.flugService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import de.chickenwings.api.ejb.FlugsucheService;
import org.slf4j.Logger;

import de.chickenwings.model.Flug;
import de.chickenwings.model.Flugzeug;
import de.chickenwings.model.types.Flugzeugtyp;
import de.chickenwings.business.util.LoggingInterceptor;

@Stateless
@Interceptors({ LoggingInterceptor.class })
public class FlugsucheServiceBean implements FlugsucheServiceLocal, FlugsucheService {

	private List<Flug> tmpFluege = new ArrayList<>();

	// ASS Persistenz-Aufgabe 6
	// @EJB
	// private FlugManagerBean flugManagerBean;

	@Inject
	private Logger logger;

	@PostConstruct
	public void initBean() {
		logger.info("XXX Erzeuge Fluege called.");
		Flug flugI = new Flug();
		flugI.setFlugnummer("XQ117");
		flugI.setStartFlughafen("CGN");
		flugI.setZielFlughafen("AYT");

		flugI.setStartzeit(LocalDateTime.of(2013, 10, 3, 3, 45, 0));
		flugI.setAnkunft(LocalDateTime.of(2013, 10, 3, 8, 15, 0));
		flugI.setFlugpreis(120d);

		Flugzeug flugzeugI = new Flugzeug();
		flugzeugI.setBesatzung(5);
		flugzeugI.setFlugzeugTyp(Flugzeugtyp.AIRBUS);
		flugzeugI.setSitzplaetze(180);

		flugI.setFlugzeug(flugzeugI);

		Flug flugII = new Flug();
		flugII.setFlugnummer("XQ 151");
		flugII.setStartFlughafen("STR");
		flugII.setZielFlughafen("AYT");
		flugII.setStartzeit(LocalDateTime.of(2013, 10, 3, 9, 35, 0));
		flugII.setAnkunft(LocalDateTime.of(2013, 10, 3, 13, 50, 0));
		flugII.setFlugpreis(230d);

		Flugzeug flugzeugII = new Flugzeug();
		flugzeugII.setBesatzung(5);
		flugzeugII.setFlugzeugTyp(Flugzeugtyp.BOING);
		flugzeugII.setSitzplaetze(240);

		flugII.setFlugzeug(flugzeugII);

		Flug flugIII = new Flug();
		flugIII.setFlugnummer("PC 4910");
		flugIII.setStartFlughafen("DTM");
		flugIII.setZielFlughafen("AYT");
		flugIII.setStartzeit(LocalDateTime.of(2013, 10, 3, 9, 30, 0));
		flugIII.setAnkunft(LocalDateTime.of(2013, 10, 3, 14, 0, 0));
		flugIII.setFlugpreis(80d);

		Flugzeug flugzeugIII = new Flugzeug();
		flugzeugIII.setBesatzung(5);
		flugzeugIII.setFlugzeugTyp(Flugzeugtyp.AIRBUS);
		flugzeugIII.setSitzplaetze(200);

		flugIII.setFlugzeug(flugzeugIII);

		Flug flugIV = new Flug();
		flugIV.setFlugnummer("XQ0161");
		flugIV.setStartFlughafen("BER");
		flugIV.setZielFlughafen("AYT");
		flugIV.setStartzeit(LocalDateTime.of(2013, 10, 3, 12, 30, 0));
		flugIV.setAnkunft(LocalDateTime.of(2013, 10, 3, 16, 50, 0));
		flugIV.setFlugpreis(310d);

		Flugzeug flugzeugIV = new Flugzeug();
		flugzeugIV.setBesatzung(5);
		flugzeugIV.setFlugzeugTyp(Flugzeugtyp.AIRBUS);
		flugzeugIV.setSitzplaetze(200);

		flugIV.setFlugzeug(flugzeugIII);

		this.tmpFluege.add(flugI);
		this.tmpFluege.add(flugII);
		this.tmpFluege.add(flugIII);
		this.tmpFluege.add(flugIV);
	}

	public List<Flug> sucheFluege() {
		return this.tmpFluege;
	}

}
