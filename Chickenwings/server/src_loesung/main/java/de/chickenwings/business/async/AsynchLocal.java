package de.chickenwings.business.async;

import java.util.concurrent.Future;

import javax.ejb.Local;

@Local
public interface AsynchLocal {
	Future<Integer> berechneWert();
}
