package de.chickenwings.business.singleton;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;

import de.chickenwings.model.Flug;
import de.chickenwings.model.Flugzeug;
import de.chickenwings.model.types.Flugzeugtyp;

@Singleton
public class FlugSingleton {

	List<Flug> alleFluege = new ArrayList<>();
	
	private int counter;

	@PostConstruct
	public void initBean() {

		Flug flugI = new Flug();
		flugI.setFlugnummer("XQ117");
		flugI.setStartFlughafen("CGN");
		flugI.setZielFlughafen("AYT");

		flugI.setStartzeit(LocalDateTime.of(2013, 10, 3, 3, 45, 0));
		flugI.setAnkunft(LocalDateTime.of(2013, 10, 3, 8, 15, 0));
		flugI.setFlugpreis(120d);

		Flugzeug flugzeugI = new Flugzeug();
		flugzeugI.setBesatzung(5);
		flugzeugI.setFlugzeugTyp(Flugzeugtyp.AIRBUS);
		flugzeugI.setSitzplaetze(180);

		flugI.setFlugzeug(flugzeugI);

		Flug flugII = new Flug();
		flugII.setFlugnummer("XQ 151");
		flugII.setStartFlughafen("STR");
		flugII.setZielFlughafen("AYT");
		flugII.setStartzeit(LocalDateTime.of(2013, 10, 3, 9, 35, 0));
		flugII.setAnkunft(LocalDateTime.of(2013, 10, 3, 13, 50, 0));
		flugII.setFlugpreis(230d);

		Flugzeug flugzeugII = new Flugzeug();
		flugzeugII.setBesatzung(5);
		flugzeugII.setFlugzeugTyp(Flugzeugtyp.BOING);
		flugzeugII.setSitzplaetze(240);

		flugII.setFlugzeug(flugzeugII);

		Flug flugIII = new Flug();
		flugIII.setFlugnummer("PC 4910");
		flugIII.setStartFlughafen("DTM");
		flugIII.setZielFlughafen("AYT");
		flugIII.setStartzeit(LocalDateTime.of(2013, 10, 3, 9, 30, 0));
		flugIII.setAnkunft(LocalDateTime.of(2013, 10, 3, 14, 0, 0));
		flugIII.setFlugpreis(80d);

		Flugzeug flugzeugIII = new Flugzeug();
		flugzeugIII.setBesatzung(5);
		flugzeugIII.setFlugzeugTyp(Flugzeugtyp.AIRBUS);
		flugzeugIII.setSitzplaetze(200);

		flugIII.setFlugzeug(flugzeugIII);

		Flug flugIV = new Flug();
		flugIV.setFlugnummer("XQ0161");
		flugIV.setStartFlughafen("BER");
		flugIV.setZielFlughafen("AYT");
		flugIV.setStartzeit(LocalDateTime.of(2013, 10, 3, 12, 30, 0));
		flugIV.setAnkunft(LocalDateTime.of(2013, 10, 3, 16, 50, 0));
		flugIV.setFlugpreis(310d);

		Flugzeug flugzeugIV = new Flugzeug();
		flugzeugIV.setBesatzung(5);
		flugzeugIV.setFlugzeugTyp(Flugzeugtyp.AIRBUS);
		flugzeugIV.setSitzplaetze(200);

		flugIV.setFlugzeug(flugzeugIII);

		alleFluege.add(flugI);
		alleFluege.add(flugII);
		alleFluege.add(flugIII);
		alleFluege.add(flugIV);

	}

	public void increment() {
		System.out.println(this);
		counter++;
	}

	public String erstelleBuchungsnummer() {
		System.out.println(this);
		return String.valueOf(new Date().getTime());
	}


	public List<Flug> getAlleFluege() {
		return alleFluege;
	}
	
	
}
