package de.chickenwings.business.logging;

import java.io.Serializable;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggerCustomFactory implements Serializable {

	private static final long serialVersionUID = 1L;

	@Produces
	public Logger createDefaultLogger(InjectionPoint ip) {
		return LoggerFactory.getLogger(ip.getMember().getDeclaringClass());
	}

}
