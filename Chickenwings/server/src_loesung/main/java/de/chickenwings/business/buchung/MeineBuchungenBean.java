package de.chickenwings.business.buchung;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateful;

import de.chickenwings.model.Buchung;
import de.chickenwings.model.Person;

@Stateful
public class MeineBuchungenBean implements MeineBuchungenLocal {

	private List<Buchung> meineBuchungen = new ArrayList<>();

	// @EJB
	// FlugSingleton flugSingleton;

	// @Inject
	// private Logger logger;

	// @EJB
	// FlugSingleton service;

	public void bucheFlug(Buchung buchung) {
		meineBuchungen.add(buchung);
	}

	public void entfernePerson(Person fluggast, Buchung buchung) {
		if (buchung ==null) {
			return;
		}
		buchung.getFluggaeste().removeIf(p -> p.equals(fluggast));
	}

	public void flugStornieren(Buchung buchung) {
		this.meineBuchungen.removeIf(b -> b.equals(buchung));
	}

	public List<Buchung> getBuchungen() {
		return meineBuchungen;
	}

}
