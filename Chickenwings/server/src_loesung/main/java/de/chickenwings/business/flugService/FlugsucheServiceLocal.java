package de.chickenwings.business.flugService;

import java.util.List;

import javax.ejb.Local;

import de.chickenwings.model.Flug;

@Local
public interface FlugsucheServiceLocal {

	List<Flug> sucheFluege();

}
